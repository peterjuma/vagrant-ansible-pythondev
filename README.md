# vagrant-ansible-pythondev

Initialize a Python development environment using Vagrant, with ansible provisioner and Jupyter Notebooks web interface.
In this configuration, we will configure a Python 3.6 development environment with Jupyter Notebooks web interface.

# Usage

```
git clone https://gitlab.com/peterjuma/vagrant-ansible-pythondev.git
cd vagrant-ansible-pythondev/
vagrant up
```

# Vagrant Configuration

Uses CentOS 7 (1 CPUs, 2GB) with private IP address, with both hostname and machine name set as python36.

# Ansible Playbook

The playbook will:
* Install IUM repository
* Enable Software Collections (SCL)
* Install Python 3.6
* Enable python3.6
* Install pip3.6
* Install multi python packages with version specifiers
* Install Jupyter
* Add and configure users with specific password
* Configure firewalld
* Generate and configure notebook password for vagrant user to run on port 8888 (Password: vagrant)

# Initialize the machine

`vagrant up`

# SSH Tunnel on vagrant localhost Using the vagrant private key

* Linux

`ssh -l vagrant -i .vagrant/machines/python36/virtualbox/private_key -p 2222 -N -L 9000:localhost:8888 localhost & `

* Powershell

`ssh -l vagrant -i .\.vagrant\machines\python36\virtualbox\private_key -p 2222 -N -L 9000:localhost:8888 localhost &`

On the browser: 

- https://localhost:9000

# SSH Tunnel using vagrant machine's private IP

`ssh -L 9000:localhost:8888 vagrant@172.28.128.50 `

- 172.28.128.50 - The private IP configured in the Vagrant file

On the browser: 

- https://172.28.128.50:9000
 
# Re-initialize Jupyter Notebook after VM restart

In case of VM restart, you can starup a Jupyter instance with the command:
 
`vagrant ssh -c nohup /usr/local/bin/jupyter notebook --no-browser &`